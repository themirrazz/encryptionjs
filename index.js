/*
Licensed under GNU General Public License 3.0
(c) Themirrazz 2022
*/
/*
Licensed under GNU General Public License 3.0
(c) Themirrazz 2022
*/

var EncryptJS = (function () {
  var e={};
  function splitBytes(data) {
    var bytes=[];
    for(var i=0;i<data.length;i++) {
      bytes.push(data.charCodeAt(i))
    }
    return bytes
  }
  function encrypt(data,key) {
    var bytes=splitBytes(data);
    var output=[];
    bytes.forEach(byte=>{
      var g=0;
      var b="";
      for(var i=0;i<key.length;i++) {
        b+=key[i].charCodeAt(i)*i;
      }
      for(var i=0;i<b.length;i++) {
        g+=(byte+b.charCodeAt(i));
      }
      output.push(g)
    })
    return output
  }
  function decrypt(data,key) {
    var output="";
    data.forEach(chunk=>{
      var g=chunk;
      var b="";
      for(var i=0;i<key.length;i++) {
        b+=key[i].charCodeAt(i)*i;
      }
      for(var i=0;i<b.length;i++) {
        g-=(b.charCodeAt(i))
      }
      g=(g/b.length)
      output+=String.fromCharCode(g)
    })
    return output
  }
  function generateCorrectionKey(length) {
    var chars="0123456789ABCDEFGKQXabcdefgkqx-_".split();
    var out="";
    for(var i=0;i<length;i++) {
      out+=chars[Math.floor(Math.random()*chars.length)];
    }
    return out
  }
  e.encrypt=function (data,key) {
    var correctionKey=generateCorrectionKey(20);
    return btoa(
      JSON.stringify({
        correctionKey:correctionKey,
        correctionTesting: encrypt(correctionKey,key),
        data: encrypt(String(data),key)
      })
    );
  }
  e.decrypt=function (data,key) {
    try {
      var obj=JSON.parse(atob(data))
    } catch (error) {
      throw new TypeError("Unable to parse the data sectors")
    }
    if(!obj.correctionKey) {
      throw new ReferenceError("could not find the correction key")
    }
    if(!obj.correctionTesting) {
      throw new ReferenceError("could not find the correction sector")
    }
    if(!obj.data) {
      throw new ReferenceError("could not find the content key")
    }
    if(obj.correctionKey!=decrypt(obj.correctionTesting,key)) {
      throw new TypeError("the key you provided is invalid");
    }
    return decrypt(obj.data,key)
  }
  return e
})()
